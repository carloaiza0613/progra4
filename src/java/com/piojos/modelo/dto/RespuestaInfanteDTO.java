/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.piojos.modelo.dto;

import com.piojos.entidades.Infante;
import java.io.Serializable;

/**
 *
 * @author Daniela
 */
public class RespuestaInfanteDTO extends RespuestaDTO
        implements Serializable {

    private Infante data;
    
    public RespuestaInfanteDTO() {
    }     
    
//   // @Override
//    public Infante getData()
//    {
//        return (Infante) super.getData();
//    }
//    
//    
//  
//    public void setData(Infante infante)
//    {
//        super.setData(infante);
//    }

    public Infante getData() {
        return data;
    }

    public void setData(Infante data) {
        this.data = data;
    }
    
    
}
