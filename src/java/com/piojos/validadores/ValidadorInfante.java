/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.piojos.validadores;

import com.piojos.controlador.InfanteFacade;
import com.piojos.entidades.Infante;
import com.piojos.excepciones.InfanteException;
import javax.ejb.EJB;

/**
 *
 * @author Daniela
 */
public class ValidadorInfante {
    
    public static void validarDatosObligatorios(Infante infante) 
            throws InfanteException 
    {
        if(infante.getIdentificacion()==null || 
                infante.getIdentificacion().equals(""))
        {
            throw new InfanteException("Debe diligenciar la identificación");
        }
        if(infante.getNombre()==null || infante.getNombre().equals(""))
        {
            throw new InfanteException("Debe diligenciar el nombre");        
        }
        if(infante.getEdad()==0 || (infante.getEdad()< 1 || infante.getEdad()>7))
        {
            throw new InfanteException("La edad debe estar en el rango 1 a 7"); 
        }
        
    }
    
    
    
    
}
