/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.piojos.servicios;

import com.piojos.entidades.Infante;
import com.piojos.excepciones.InfanteException;
import com.piojos.modelo.dto.RespuestaDTO;
import com.piojos.modelo.dto.RespuestaInfanteDTO;
import com.piojos.validadores.ValidadorInfante;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Daniela
 */
@Stateless
@Path("com.piojos.entidades.infante")
public class InfanteFacadeREST extends AbstractFacade<Infante> {

    @PersistenceContext(unitName = "piojosPU")
    private EntityManager em;

    public InfanteFacadeREST() {
        super(Infante.class);
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    public RespuestaDTO createInfante(Infante entity) {

        RespuestaInfanteDTO respuesta = new RespuestaInfanteDTO();
        try {
            ValidadorInfante.validarDatosObligatorios(entity);
            //Validar si existe
            if (super.find(entity.getIdentificacion()) == null) {
                // ValidadorInfante.validarExistenciaInfante(entity.getIdentificacion());
                super.create(entity);
                respuesta.setCodigo("200");
                respuesta.setExito(true);
                respuesta.setMensaje("Guardado con éxito");
                respuesta.setData(entity);
            } else {
                respuesta.setCodigo("301");
                respuesta.setExito(false);
                respuesta.setMensaje("Ya existe un infante con"
                        + "la identificación " + entity.getIdentificacion());
            }
        } catch (InfanteException ex) {
            respuesta.setCodigo("301");
            respuesta.setExito(false);
            respuesta.setMensaje(ex.getMessage());
        }

        return respuesta;
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") String id, Infante entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") String id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Infante find(@PathParam("id") String id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_JSON})
    public List<Infante> findAll() {
        return super.findAll();
    }
    /*
    
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public RespuestaDTO findAllInfantes() {
        RespuestaDTO respuesta = new RespuestaDTO();
        List<Infante> lista = super.findAll();
        if (lista.isEmpty()) {
            respuesta.setCodigo("301");
            respuesta.setExito(false);
            respuesta.setMensaje("No existen datos");
            //respuesta.setData(lista);
        } else {
            respuesta.setCodigo("200");
            respuesta.setExito(true);
            respuesta.setMensaje("Consulta exitosa");
            respuesta.setData(lista);
        }
        return respuesta;
    }
*/
    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Infante> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
