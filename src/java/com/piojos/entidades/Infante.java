/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.piojos.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Daniela
 */
@Entity
@Table(name = "infante")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Infante.findAll", query = "SELECT i FROM Infante i")
    , @NamedQuery(name = "Infante.findByNombre", query = "SELECT i FROM Infante i WHERE i.nombre = :nombre")
    , @NamedQuery(name = "Infante.findByEdad", query = "SELECT i FROM Infante i WHERE i.edad = :edad")
    , @NamedQuery(name = "Infante.findByIdentificacion", query = "SELECT i FROM Infante i WHERE i.identificacion = :identificacion")})
public class Infante implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "edad")
    private short edad;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "identificacion")
    private String identificacion;
    @JoinColumn(name = "fk_municipio", referencedColumnName = "codigo")
    @ManyToOne(optional = false)
    private Municipio fkMunicipio;

    public Infante() {
    }

    public Infante(String identificacion) {
        this.identificacion = identificacion;
    }

    public Infante(String identificacion, String nombre, short edad) {
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public short getEdad() {
        return edad;
    }

    public void setEdad(short edad) {
        this.edad = edad;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public Municipio getFkMunicipio() {
        return fkMunicipio;
    }

    public void setFkMunicipio(Municipio fkMunicipio) {
        this.fkMunicipio = fkMunicipio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (identificacion != null ? identificacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Infante)) {
            return false;
        }
        Infante other = (Infante) object;
        if ((this.identificacion == null && other.identificacion != null) || (this.identificacion != null && !this.identificacion.equals(other.identificacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.piojos.entidades.Infante[ identificacion=" + identificacion + " ]";
    }
   
}
