/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.piojos.excepciones;

/**
 *
 * @author Daniela
 */
public class InfanteException extends Exception{

    public InfanteException(String string) {
        super(string);
    }

    public InfanteException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public InfanteException(Throwable thrwbl) {
        super(thrwbl);
    }
    
}
